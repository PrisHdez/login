package com.login.dao;

import com.login.entity.Usuario;

public interface UsuarioDao extends GenericDao<Usuario, Long> {
	
	Usuario findUserServicesByUserName(String userName);

}
