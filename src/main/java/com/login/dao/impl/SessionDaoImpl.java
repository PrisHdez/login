package com.login.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.login.dao.GenericDao;

@Repository
public class SessionDaoImpl <TYPE extends Serializable, PK extends Serializable> extends HibernateDaoSupport implements GenericDao<TYPE, PK>{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Class<TYPE> persistenceClass;

    public void setSessionFactoryPrimario(SessionFactory sessionFactory) {
    	  super .setSessionFactory(sessionFactory);
    }
	
	public SessionDaoImpl() {
	}

    @SuppressWarnings({ "unchecked" })
    public PK save(TYPE toCreate) {
        PK id = null;
        id = (PK) this.getHibernateTemplate().save(toCreate);
        this.getHibernateTemplate().flush();
        return id;
    };

    @Override
    public void delete(TYPE toDelete) {
        this.getHibernateTemplate().delete(toDelete);
        this.getHibernateTemplate().flush();
    }

    @Override
    public void update(TYPE transientObject) {
        this.getHibernateTemplate().update(transientObject);
    }

    @Override
    public TYPE find(PK primaryKey) {
        return (TYPE) this.getHibernateTemplate().get(getPersistenceClass(), primaryKey);
    }
    
    @SuppressWarnings("unchecked")
    public Class<TYPE> getPersistenceClass() {
        if (persistenceClass == null) {
            Class<?> clazz = getClass();

            while (!(clazz.getGenericSuperclass() instanceof ParameterizedType)) {
                clazz = clazz.getSuperclass();
            }
            persistenceClass = (Class<TYPE>) ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];
        }
        return persistenceClass;
    }

}
