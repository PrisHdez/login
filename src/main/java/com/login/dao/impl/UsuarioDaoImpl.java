package com.login.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.login.dao.UsuarioDao;
import com.login.entity.Usuario;

@Repository
public class UsuarioDaoImpl extends SessionDaoImpl<Usuario, Long> implements UsuarioDao {
	
	private Logger logger = LoggerFactory.getLogger(UsuarioDaoImpl.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	public Usuario findUserServicesByUserName(String userName) {
		Usuario usuario = null;

			List<Usuario> usuarioLst = null;
			String consulta = "select usu from Usuario usu " +
					"where usu.email = '" + userName + "' and usu.status = 1";
			
			usuarioLst = (List<Usuario>) getHibernateTemplate().find(consulta);									
		
			if (usuarioLst != null && !usuarioLst.isEmpty()) {
				usuario = usuarioLst.get(0);
			}
		
		return usuario;
	}

	@Override
	public Long save(Usuario toCreate) {
		return null;
	}

	@Override
	public void delete(Usuario toDelete) {
		
	}

	@Override
	public void update(Usuario toUpdate) {
		
	}

	@Override
	public Usuario find(Long primaryKey) {
		return null;
	}

}
