package com.login.dao;

import java.io.Serializable;

public interface GenericDao<TYPE extends Serializable, PK extends Serializable> extends Serializable {

	PK save(TYPE toCreate);
	
	void delete(TYPE toDelete);
	
	void update(TYPE toUpdate);
	
	TYPE find(PK primaryKey);
	
}
