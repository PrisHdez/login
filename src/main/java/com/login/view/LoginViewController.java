package com.login.view;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;

@ManagedBean
@ViewScoped
public class LoginViewController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Logger logger = LoggerFactory.getLogger(LoginViewController.class);
	
	private String userInput;
	private String passInput;
	
	@PostConstruct
	public void init() {
		logger.info("Inicializando recursos");
		
	}
	
    public LoginViewController() {

        Exception ex = (Exception) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebAttributes.AUTHENTICATION_EXCEPTION);

        if (ex != null) {

            if (ex.getMessage().contains("BlockedUserException")) {
            	logger.info("user blocked");
            } else {
            	logger.info("user incorrect pass ");
            }
        }
    }
    
    public void login(){
    	logger.info("login");
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("/ControlUsuarios/" + "j_spring_security_check?j_username="+ userInput + "&j_password=" + passInput);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    }

	public String getUserInput() {
		return userInput;
	}

	public void setUserInput(String userInput) {
		this.userInput = userInput;
	}

	public String getPassInput() {
		return passInput;
	}

	public void setPassInput(String passInput) {
		this.passInput = passInput;
	}
    
}
