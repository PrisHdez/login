package com.login.view;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.WebAttributes;

public class SecurityController implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7918430924466303742L;
	
	private Logger logger = LoggerFactory.getLogger(SecurityController.class);
	
    private String userInput;
    private String passInput;
	
    public SecurityController() {

        Exception ex = (Exception) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(WebAttributes.AUTHENTICATION_EXCEPTION);

        if (ex != null) {

            if (ex.getMessage().contains("BlockedUserException")) {
            	logger.info("user blocked");
            } else {
            	logger.info("user incorrect pass ");
            }
        }
    }
    
    public void login(ActionEvent e) throws java.io.IOException {
		FacesContext.getCurrentInstance().getExternalContext().redirect("/ControlUsuarios/" + "j_spring_security_check?j_username="+ userInput + "&j_password=" + passInput);
    }

}
