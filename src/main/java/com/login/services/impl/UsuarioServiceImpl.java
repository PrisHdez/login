package com.login.services.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.login.builder.UsuarioBuilder;
import com.login.dao.UsuarioDao;
import com.login.dto.UsuarioDTO;
import com.login.entity.Usuario;
import com.login.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	
	private Logger logger = LoggerFactory.getLogger(UsuarioServiceImpl.class);

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Resource
	private UsuarioDao usuarioDao;
	
	@Override
	public UsuarioDTO buscarUsuarioActivoPor(String userName) {
		logger.info("Buscando usuario por {}",userName);
		return UsuarioBuilder.convertEntityToDTO(usuarioDao.findUserServicesByUserName(userName));
	}

	@Override
	public List<UsuarioDTO> buscarUsuariosPor(Usuario usuario) {
		// TODO Auto-generated method stub
		return null;
	}



}
