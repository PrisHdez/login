package com.login.services.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.login.dto.UsuarioDTO;
import com.login.security.UserCredentials;
import com.login.service.UsuarioService;

@Service
public class UsuarioAuthenticationProvider  implements UserDetailsService {
	
	private Logger logger = LoggerFactory.getLogger(UsuarioAuthenticationProvider.class);
	
	@Resource
    private UsuarioService usuarioService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		logger.info("Buscando usuario {}",username);
		User userDetails = null;
		final List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		authList.add(new SimpleGrantedAuthority("ROL_SYSTEM_CONSOLE"));	//por asi decirlo hay que ver si tendremos roles
		
		UsuarioDTO founded = findThisUser(username);   
		
		userDetails = new UserCredentials(founded.getEmail(), founded.getPassword(), true, true, true, true, authList, founded);
		
		return userDetails;
	}
	
	private UsuarioDTO findThisUser(String username) throws DataAccessException {
		UsuarioDTO toReturn = null;
		if (username == null) {
			throw new UsernameNotFoundException("No se ha encontrado el usuario que se desea logear. usuario es nulo.");
		} else {
			logger.info("Buscando usuario por servicio {}",username);
			toReturn = usuarioService.buscarUsuarioActivoPor(username);
		}

		return toReturn;
	}

}
