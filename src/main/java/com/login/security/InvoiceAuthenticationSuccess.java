package com.login.security;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class InvoiceAuthenticationSuccess implements AuthenticationSuccessHandler {
	
	private static Logger logger = LoggerFactory.getLogger(InvoiceAuthenticationSuccess.class);

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,Authentication authentication) throws IOException, ServletException {
		logger.info("Is correct");
		Set<String> roles = AuthorityUtils.authorityListToSet(authentication.getAuthorities());
		
		response.sendRedirect(request.getContextPath() + "/jsp/holamundo.xhtml");
	}

}
