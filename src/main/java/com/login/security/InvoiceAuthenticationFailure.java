package com.login.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class InvoiceAuthenticationFailure implements AuthenticationFailureHandler {

	private static Logger logger = LoggerFactory.getLogger(InvoiceAuthenticationFailure.class);

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		logger.info("Usuario/Password Incorrecto");
		response.sendRedirect("/ControlUsuarios/login.xhtml");	
		
	}
	
}
