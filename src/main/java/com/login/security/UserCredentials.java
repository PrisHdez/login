package com.login.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.login.dto.UsuarioDTO;

public class UserCredentials extends User {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private UsuarioDTO datosUsuario;
	
	public UserCredentials(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, UsuarioDTO datosUsuario) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.datosUsuario = datosUsuario;
	}

	public UsuarioDTO getDatosUsuario() {
		return datosUsuario;
	}

	public void setDatosUsuario(UsuarioDTO datosUsuario) {
		this.datosUsuario = datosUsuario;
	}
	

}
