package com.login.service;

import java.io.Serializable;
import java.util.List;

import com.login.dto.UsuarioDTO;
import com.login.entity.Usuario;

public interface UsuarioService extends Serializable {
	
	UsuarioDTO buscarUsuarioActivoPor(String userName);
	
	List<UsuarioDTO> buscarUsuariosPor(Usuario usuario);

}
