package com.login.builder;

import com.login.dto.UsuarioDTO;
import com.login.entity.Usuario;

public class UsuarioBuilder {

	public static UsuarioDTO convertEntityToDTO(Usuario usuario) {
		UsuarioDTO usuarioDTO = new UsuarioDTO();

		if (usuario != null) {
			usuarioDTO.setIdUsuario(usuario.getIdUsuario());
			usuarioDTO.setEmail(usuario.getEmail());
			usuarioDTO.setPassword(usuario.getPassword());
			usuarioDTO.setStatus(usuario.getStatus());
			usuarioDTO.setNombre(usuario.getNombre());
			usuarioDTO.setApePaterno(usuario.getApePaterno());
			usuarioDTO.setApeMaterno(usuario.getApeMaterno());
		} else {
			return null;

		}//if-else
		
		return null;
	}
	
	public static Usuario convertDTOToEntity(UsuarioDTO usuarioDTO) {
		Usuario usuario = new Usuario();

		if (usuarioDTO != null) {
			usuario.setIdUsuario(usuarioDTO.getIdUsuario());
			usuario.setEmail(usuarioDTO.getEmail());
			usuario.setPassword(usuarioDTO.getPassword());
			usuario.setStatus(usuarioDTO.getStatus());
			usuario.setNombre(usuarioDTO.getNombre());
			usuario.setApePaterno(usuarioDTO.getApePaterno());
			usuario.setApeMaterno(usuarioDTO.getApeMaterno());
		} else {
			return null;

		}//if-else
		
		return null;
	}
}
